-r requirements.txt
flake8
# pytest will be brought in by pytest-cov
pytest-cov
sphinx==1.5.3
testfixtures
httpretty
